Include(FetchContent)

FetchContent_Declare(
  Catch2
  GIT_REPOSITORY https://github.com/catchorg/Catch2.git
  GIT_TAG        v3.4.0 # or a later release
)

FetchContent_MakeAvailable(Catch2)

include_directories(${PROJECT_SOURCE_DIR}/src)

add_executable(fibonacci_test_catch fibonacci_test.cpp)
target_link_libraries(fibonacci_test_catch PUBLIC  fibonacci
                                           PRIVATE Catch2::Catch2WithMain)

add_test(NAME Fibonacci-Test-with-Catch COMMAND fibonacci_test_catch)
