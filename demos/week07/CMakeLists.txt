cmake_minimum_required(VERSION 3.15)

project(PT1_week07)

# use C++11 (globally)
set(CMAKE_CXX_STANDARD 11)          # set standard to C++11
set(CMAKE_CXX_STANDARD_REQUIRED ON) # requires C++11
set(CMAKE_CXX_EXTENSIONS OFF)       # disables extensions such as -std=g++XX

# setting warning compiler flags
add_compile_options(-Wall -Wextra -Wpedantic)

add_executable(list_find list_find.cpp)
add_executable(maps maps.cpp)
add_executable(modif modif.cpp)
add_executable(non_modif non_modif.cpp)
add_executable(pair pair.cpp)
add_executable(sequence_containers sequence_containers.cpp)
add_executable(vector_grow vector_grow.cpp)
