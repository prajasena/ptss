#include <type_traits>

template <typename F, typename = void>
struct result_t {
  using type = double;
};

template <typename R, typename T>
struct result_t<R (*)(T)> {
  using type = R;
};

template <typename F>
struct result_t<F, std::void_t<typename F::output_t>> {
  using type = typename F::output_t;
};

template <typename F, typename = void>
struct domain_t {
  using type = double;
};

template <typename R, typename T>
struct domain_t<R (*)(T)> {
  using type = T;
};

template <typename F>
struct domain_t<F, std::void_t<typename F::output_t>> {
  using type = typename F::output_t;
};

template <typename F>
typename result_t<F>::type integrate(typename domain_t<F>::type from,
                                     typename domain_t<F>::type to,
                                     unsigned bins, F f);

template <typename F>
typename result_t<F>::type integrate(typename domain_t<F>::type from,
                                     typename domain_t<F>::type to,
                                     unsigned bins, F f) {
  // assert(bins > 0);
  using dom_t = typename domain_t<F>::type;
  using res_t = typename result_t<F>::type;

  res_t sum = 0;
  const dom_t del_x = (to - from) / (dom_t)bins;

  for (unsigned int i = 0; i < bins; ++i) {
    const dom_t x_i = from + (dom_t)i * del_x;
    sum += f(x_i);
    sum += (res_t)4 * f(x_i + del_x / (dom_t)2);
    sum += f(x_i + del_x);
  }

  return (res_t)del_x / (res_t)6 * sum;
}