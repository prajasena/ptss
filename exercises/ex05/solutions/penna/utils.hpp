/**
 * Header for the Penna utils (i.e., general-purpose utility functions).
 * Programming Techniques for Scientific Simulations
 */

#ifndef UTILS_HPP
#define UTILS_HPP

// Penna namespace.
namespace Penna {

double drand();

} // namespace Penna

#endif // UTILS_HPP
