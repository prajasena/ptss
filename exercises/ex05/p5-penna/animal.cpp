#include "animal.h"

Animal::Animal(Genome& _genome) : genome(_genome) {}

void Animal::increase_age(unsigned population, unsigned max_population) {
  if (is_dead) return;
  ++age;

  bad_mutation_ctr += genome[age];

  if (bad_mutation_ctr >= bad_mutation_threshold ||
      max_population - population <= rand() % max_population) {
    is_dead = true;
    return;
  }

  gave_birth = age > reproductive_age;
}

Animal Animal::reprodruce() {
  Genome new_genome = genome.reproduce();
  return Animal(new_genome);
}