#pragma once

#include "genome.h"

class Animal {
 private:
  unsigned age;
  Genome genome;
  unsigned bad_mutation_ctr;

 public:
  /// @brief number of accumulated bad mutations an individual can survive
  static constexpr unsigned bad_mutation_threshold = 10;

  /// @brief age after which animal can reproduce
  static constexpr unsigned reproductive_age = 3;

  /// @brief construct animal from genome
  /// @param genome
  Animal(Genome& genome);

  /// @brief true if animal gave birth after aging last year
  bool gave_birth;

  /// @brief true if animal has died
  bool is_dead;

  /// @brief increase the age of the animal, let it die if the conditions are
  /// met and give birth
  /// @param population curr population
  /// @param max_population maximum number of individuals in population
  void increase_age(unsigned population, unsigned max_population);

  /// @brief call `reproduce` on the genome and call the constructor on it
  Animal reprodruce();
};