#include <cassert>
#include <cmath>
#include <iostream>

template <typename F>
double integrate(F f, double from, double to, unsigned bins) {
  assert(bins > 0);

  double sum = 0;
  const double del_x = (to - from) / bins;

  for (unsigned int i = 0; i < bins; ++i) {
    const double x_i = from + i * del_x;
    sum += f(x_i);
    sum += 4 * f(x_i + del_x / 2);
    sum += f(x_i + del_x);
  }

  return del_x / 6 * sum;
}

template <typename Func>
double integrate3d(Func f, double a, double b, double c, double d,
                   unsigned bins_x, unsigned bins_y) {
  const auto F_x = [&f, c, d, bins_y](double x) {
    const auto f_y = [x, &f, c, d, bins_y](double y) { return f(x, y); };

    return integrate(f_y, c, d, bins_y);
  };

  return integrate(F_x, a, b, bins_x);
}

bool test_unit_disk() {
  auto unit_disk = [](double x, double y) {
    return (x * x + y * y <= 1) * 1.0;
  };

  double area = integrate3d(unit_disk, -1, 1, -1, 1, 200, 200);
  return std::abs(area - M_PI) <= 1e-3;
}

int main() {
  assert(test_unit_disk());

  constexpr double R = 1.7606;

  auto density = [R](double x, double y) {
    return (x * x + y * y <= R * R) ? std::exp(-(x * x + y * y)) : 0.0;
  };

  std::cout << integrate3d(density, -R, R, -R, R, 100, 100);

  return 0;
}
