#include <iomanip>
#include <iostream>

#include "p2-simpson.h"

int main() {
  double (*f)(double) = std::sin;

  for (unsigned i = 0; i < 10; ++i) {
    const unsigned bins = 2 << i;
    std::cout << std::setw(6) << bins << std::setw(15) << std::setprecision(8)
              << integrate(f, 0, M_PI, bins) << std::endl;
  }
}