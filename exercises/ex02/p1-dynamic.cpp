#include <iostream>
#include <vector>

int main() {
  unsigned n;
  std::cin >> n;
  std::vector<double> numbers;
  numbers.reserve(n);

  double sum = 0;
  for (unsigned i = 0; i < n; ++i) {
    std::cin >> numbers[i];
    sum += numbers[i];
  }

  for (int i = n - 1; i >= 0; --i) {
    std::cout << numbers[i] / sum << " ";
  }
}