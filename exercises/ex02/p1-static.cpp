#include <iostream>
#define N_MAX 128

int main() {
  unsigned n;
  std::cin >> n;
  double numbers[N_MAX];

  if (n > N_MAX) {
    std::cerr << "input too large";
    return -1;
  }

  double sum = 0;
  for (unsigned i = 0; i < n; ++i) {
    std::cin >> numbers[i];
    sum += numbers[i];
  }

  for (int i = n - 1; i >= 0; --i) {
    std::cout << numbers[i] / sum << " ";
  }
}