#pragma once

// integrates f on the integral [from, to] using the simpsons method
// PRE: f must be a non null function pointer double -> double
double integrate(double (*f)(double), double from, double to, unsigned bins);