class Func {
  virtual double operator()(const double) {};
};

double integrate(Func f, double from, double to, unsigned bins) {
  double sum = 0;
  const double del_x = (to - from) / bins;

  for (unsigned int i = 0; i < bins; ++i) {
    const double x_i = from + i * del_x;
    sum += f(x_i);
    sum += 4 * f(x_i + del_x / 2);
    sum += f(x_i + del_x);
  }

  return del_x / 6 * sum;
}