#include <iostream>

class A {
 public:
  virtual void f() const { std::cout << "A::f "; }
  void g() const { std::cout << "A::g "; }
};

class B : public A {
 public:
  void f() const { std::cout << "B::f "; }
  void g() { std::cout << "B::g "; }
};

class C : public B {
 public:
  void f() { std::cout << "C::f "; }
  void g() const { std::cout << "C::g "; }
};

void func(A const& a) {
  a.f();
  a.g();
}

int main() {
  A a;
  B b;
  C c;

  a.f();  // A::f
  a.g();  // A::g

  b.f();  // B::f
  b.g();  // B::g

  c.f();  // C::f
  c.g();  // C::g

  func(a);  // A::f A::g
  func(b);  // B::f A::g
  func(c);  // B::f A::g

  std::cout << std::endl;
}
