#include "population.h"

#include <ranges>

#include "animal.h"

Population::Population(unsigned _max_population)
    : individuals({}), max_population(_max_population) {}

size_t Population::size() { return individuals.size(); }

void Population::add_individual(Animal individual) {
  individuals.push_back(individual);
}

void Population::increase_age() {
  unsigned population_size = individuals.size();
  std::vector<Animal> newborns;

  for (unsigned i = 0; i < population_size; ++i) {
    Animal& individual = individuals[i];
    individual.increase_age(population_size, max_population, fishing_prob);

    if (individual.gave_birth) individuals.push_back(individual.reproduce());
  }

  remove_dead();
}

void Population::remove_dead() {
  auto new_individuals = individuals | std::ranges::views::filter(
                                           [](Animal a) { return !a.is_dead; });
  individuals =
      std::vector<Animal>(new_individuals.begin(), new_individuals.end());
}

void Population::setFishingProb(unsigned _fishing_prob) {
  fishing_prob = _fishing_prob;
}