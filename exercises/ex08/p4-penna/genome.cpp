#include "genome.h"

unsigned Genome::genome_length = 128;
unsigned Genome::mutation_rate = 10;

Genome::Genome() : bits(0) {}

Genome::Genome(genome_bits _bits) : bits(_bits) {}

Genome::Genome(const Genome& g) : bits(g.bits) {}

genome_bits Genome::get_bits() { return bits; }

bool Genome::operator[](unsigned i) {
  assert(i < genome_length);
  return ((1 << i) & bits) > 0;
}

unsigned Genome::mutations() {
  unsigned count = 0;
  genome_bits mask = 1;

  for (unsigned i = 0; i < genome_length; ++i) {
    count += (bits & mask) > 0;
    mask <<= 1;
  }

  return count;
}

genome_bits random_genome_bits(unsigned length) {
  constexpr unsigned segment_len = 16;
  const unsigned segments = (length + segment_len - 1) / segment_len;
  genome_bits rand_str = 0;

  for (unsigned i = 0; i < segments; ++i) {
    const genome_bits rand_segment = rand() % (1 << segment_len);
    rand_str |= rand_segment << (i * segment_len);
  }

  return rand_str;
}

Genome Genome::reproduce() {
  unsigned rand_bit_pos_set = 0;
  genome_bits rand_bits_pos_mask = 0;

  while (rand_bit_pos_set < mutation_rate) {
    const unsigned rand_pos = rand() % genome_length;

    if ((rand_bits_pos_mask & (1 << rand_pos)) > 0) continue;

    rand_bits_pos_mask |= (1 << rand_pos);
    ++rand_bit_pos_set;
  }

  genome_bits rand_str = random_genome_bits(genome_length);

  return Genome(bits ^ (rand_str & rand_bits_pos_mask));
}