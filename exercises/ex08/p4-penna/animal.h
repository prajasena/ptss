#pragma once

#include "genome.h"

class Animal {
 private:
  unsigned age = 0;
  Genome genome;
  unsigned bad_mutation_ctr = 0;

 public:
  /// @brief number of accumulated bad mutations an individual can survive
  static unsigned bad_mutation_threshold;

  /// @brief age after which animal can reproduce
  static unsigned reproductive_age;

  /// @brief copy constructor
  /// @param animal
  Animal(const Animal& animal);

  /// @brief construct animal from genome
  /// @param genome
  Animal(Genome& genome);

  /// @brief true if animal gave birth after aging last year
  bool gave_birth;

  /// @brief true if animal has died
  bool is_dead;

  /// @brief increase the age of the animal, let it die if the conditions are
  /// met and give birth
  /// @param population curr population
  /// @param max_population maximum number of individuals in population
  void increase_age(unsigned population, unsigned max_population,
                    unsigned fishing_prob);

  /// @brief call `reproduce` on the genome and call the constructor on it
  Animal reproduce();
};