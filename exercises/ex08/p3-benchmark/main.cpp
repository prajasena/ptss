#include <iomanip>
#include <iostream>

#include "math.h"
#include "simpson.h"
#include "timer.h"

#define DEF_F(n, f)                                             \
  double f_func_##n(const double x) { return f(x); }            \
  struct f_trait_##n {                                          \
    using output_t = double;                                    \
    using input_t = double;                                     \
    output_t operator()(const input_t x) const { return f(x); } \
  };                                                            \
  class f_class_##n : public Func {                             \
   public:                                                      \
    double operator()(const double x) const { return f(x); }    \
  };

#define TIME_START(n) \
  Timer timer_##n;    \
  timer_##n.start();

#define TIME_END(n) \
  timer_##n.stop(); \
  t##n = timer_##n.duration();

#define BENCH(n)                                  \
  {                                               \
    double t1, t2, t3, t4;                        \
    double v1, v2, v3, v4;                        \
    TIME_START(1)                                 \
    v1 = integrate1(f_##n, from, to, bins);       \
    TIME_END(1)                                   \
    TIME_START(2)                                 \
    v2 = integrate2(*f_func_##n, from, to, bins); \
    TIME_END(2)                                   \
    f_trait_##n f_trait;                          \
    TIME_START(3)                                 \
    v3 = integrate3(f_trait, from, to, bins);     \
    TIME_END(3)                                   \
    f_class_##n f_class;                          \
    TIME_START(4)                                 \
    v4 = integrate4(f_class, from, to, bins);     \
    TIME_END(4)                                   \
    times.push_back({t1, t2, t3, t4});            \
    values.push_back({v1, v2, v3, v4});           \
  }

template <typename F1, typename F3, typename F4>
void benchmark(double from, double to, unsigned bins, F1 f_obj,
               double (*f_ptr)(double), F3 f_trait, F4 f_class) {
  integrate1(f_obj, from, to, bins);
  integrate2(f_ptr, from, to, bins);
  integrate3(f_trait, from, to, bins);
  integrate4(f_class, from, to, bins);
}

#define f_1(x) 0
#define f_2(x) 1
#define f_3(x) x
#define f_4(x) x* x
#define f_5(x) std::sin(x)
#define f_6(x) std::sin(5 * x)

DEF_F(1, f_1)
DEF_F(2, f_2)
DEF_F(3, f_3)
DEF_F(4, f_4)
DEF_F(5, f_5)
DEF_F(6, f_6)

int main() {
  constexpr double from = -1;
  constexpr double to = 1;
  constexpr unsigned bins = 1e8;

  std::vector<std::vector<double>> times;
  std::vector<std::vector<double>> values;

  BENCH(1)
  BENCH(2)
  BENCH(3)
  BENCH(4)
  BENCH(5)
  BENCH(6)

  std::cout << "values:\n\n"
            << std::setw(16) << "f" << std::setw(16) << "hard-coded"
            << std::setw(16) << "pointer" << std::setw(16) << "template"
            << std::setw(16) << "class" << std::endl;
  for (unsigned i = 0; i < 6; ++i) {
    std::cout << std::setw(16) << i + 1 << std::setw(16) << values[i][0]
              << std::setw(16) << values[i][1] << std::setw(16) << values[i][2]
              << std::setw(16) << values[i][3] << std::endl;
  }

  std::cout << "\n\nspeed:\n\n"
            << std::setw(16) << "f" << std::setw(16) << "hard-coded"
            << std::setw(16) << "pointer" << std::setw(16) << "template"
            << std::setw(16) << "class" << std::endl;
  for (unsigned i = 0; i < 6; ++i) {
    std::cout << std::setw(16) << i + 1 << std::setw(16) << times[i][0]
              << std::setw(16) << times[i][1] << std::setw(16) << times[i][2]
              << std::setw(16) << times[i][3] << std::endl;
  }
}