#include <cassert>
#include <iostream>

#define integrate1(f, from, to, bins)         \
  ({                                          \
    double sum = 0;                           \
    const double del_x = (to - from) / bins;  \
    for (unsigned int i = 0; i < bins; ++i) { \
      const double x_i = from + i * del_x;    \
      sum += f(x_i);                          \
      sum += 4 * f(x_i + del_x / 2);          \
      sum += f(x_i + del_x);                  \
    }                                         \
    const double res = del_x / 6 * sum;       \
    res;                                      \
  })

double integrate2(double (*f)(double), double from, double to, unsigned bins) {
  assert(f != nullptr);

  double sum = 0;
  const double del_x = (to - from) / bins;

  for (unsigned int i = 0; i < bins; ++i) {
    const double x_i = from + i * del_x;
    sum += f(x_i);
    sum += 4 * f(x_i + del_x / 2);
    sum += f(x_i + del_x);
  }

  return del_x / 6 * sum;
}

template <typename F, typename = void>
struct result_t {
  using type = double;
};

template <typename R, typename T>
struct result_t<R (*)(T)> {
  using type = R;
};

template <typename F>
struct result_t<F, std::void_t<typename F::output_t>> {
  using type = typename F::output_t;
};

template <typename F, typename = void>
struct domain_t {
  using type = double;
};

template <typename R, typename T>
struct domain_t<R (*)(T)> {
  using type = T;
};

template <typename F>
struct domain_t<F, std::void_t<typename F::output_t>> {
  using type = typename F::output_t;
};

template <typename F>
typename result_t<F>::type integrate3(F f, typename domain_t<F>::type from,
                                      typename domain_t<F>::type to,
                                      unsigned bins) {
  // assert(bins > 0);
  using dom_t = typename domain_t<F>::type;
  using res_t = typename result_t<F>::type;

  res_t sum = 0;
  const dom_t del_x = (to - from) / (dom_t)bins;

  for (unsigned int i = 0; i < bins; ++i) {
    const dom_t x_i = from + (dom_t)i * del_x;
    sum += f(x_i);
    sum += (res_t)4 * f(x_i + del_x / (dom_t)2);
    sum += f(x_i + del_x);
  }

  return (res_t)del_x / (res_t)6 * sum;
}

class Func {
 public:
  virtual double operator()(const double x) const {}
};

double integrate4(const Func& f, double from, double to, unsigned bins) {
  double sum = 0;
  const double del_x = (to - from) / bins;

  for (unsigned int i = 0; i < bins; ++i) {
    const double x_i = from + i * del_x;
    sum += f(x_i);
    sum += 4 * f(x_i + del_x / 2);
    sum += f(x_i + del_x);
  }

  return del_x / 6 * sum;
}