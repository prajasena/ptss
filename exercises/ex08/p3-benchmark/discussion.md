I compiled `main.cpp` using

```sh
/usr/bin/g++ -fdiagnostics-color=always -std=c++20 -O3 -DNDEBUG -march=native -funroll-loops -g p3-benchmark/*.cpp -o p3-benchmark/_build/main
```

and I got

```
f      hard-coded         pointer        template           class
1         8.4e-08         4.1e-08         4.2e-08        0.348322
2        0.283154        0.281486        0.285002        0.314877
3        0.287824        0.317953        0.319936        0.314842
4        0.312825        0.314521        0.314793        0.324749
5        0.642872        0.641034         0.63964        0.723538
6        0.895095        0.914641        0.918018         1.00478
```

All methods are pretty much identical in speed except for the class based method which seems to have an significant overhead for when using it with a "simple" function; for complexer ones the relative difference gets smaller. I think this might be due to the fact that the virtual function causes some kind of pointer lookup at runtime. If this was done at compile-tme, the compiler could do many optimization (incl- computing everything at compile-time as bounds and bins were given as `constexpr`).