#ifndef UTILS_HPP
#define UTILS_HPP

// Penna namespace.
namespace Penna {

double drand();

} // namespace Penna

#endif // UTILS_HPP
