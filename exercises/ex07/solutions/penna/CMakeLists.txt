cmake_minimum_required(VERSION 3.15)

project(Penna)

# use C++11 (globally)
set(CMAKE_CXX_STANDARD 11)

set(DIRECTORIES src test)

# This needs to be in both the top-level and the test CMake file.
enable_testing()

foreach(directory ${DIRECTORIES})
  add_subdirectory(${directory})
endforeach(directory)

add_executable(penna_sim main.cpp)
target_link_libraries(penna_sim penna)
