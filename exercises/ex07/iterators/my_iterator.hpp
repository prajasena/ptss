#ifndef MY_ITERATOR_HPP
#define MY_ITERATOR_HPP

#include <cassert>
#include <iostream>  // for debugging
#include <iterator>  // for iterator category tags

/*
Forward & bidirectional iterators requirements:

Iterator:
- CopyConstructible
- CopyAssignable
- Destructible
- Supports: *a (Dereferenceable)
- Supports: ++a (Preincrementable)

Input Iterator:
- All requirements of an iterator.
- Supports: == (EqualityComparable)
- Supports: !=
- Supports: ->
- Supports: a++ (Postincrementable)

Forward Iterator:
- All requirements of an input iterator
- DefaultConstructible
- Supports expression: *a++

Bidirectional Iterator:
- All requirements of a forward iterator
- Predecrementable
- Postdecrementable
- Supports expression: *a--

*/

// my iterator
template <typename T>
class MyIterator {
 public:
  // member types
  using value_type = T;  // type of values obtained when dereferencing the
                         // iterator
  using difference_type = std::size_t;  // signed integer type to represent
                                        // distance between iterators
  using reference = T&;                 // type of reference to type of values
  using pointer = T*;                   // type of pointer to the type of values
  // using iterator_category = std::forward_iterator_tag;        // category of
  //                                                             // the iterator
  using iterator_category = std::bidirectional_iterator_tag;  // category of
                                                              // the iterator

  // constructor
  MyIterator();

  MyIterator(pointer first_elem, pointer last_elem, bool init_last);

  // copy ctor
  MyIterator(MyIterator const&) = default;
  // copy assignment
  MyIterator& operator=(MyIterator const&) = default;
  // dtor
  ~MyIterator() {}

  reference operator*() { return *curr_elem; }

  bool operator!=(MyIterator other) { return other.curr_elem != curr_elem; };

  MyIterator operator++() {
    assert(curr_elem != last_elem);
    ++curr_elem;
    return *this;
  }

  MyIterator operator--() {
    assert(curr_elem != first_elem);
    --curr_elem;
    return *this;
  }

 private:
  pointer curr_elem;
  pointer first_elem;
  pointer last_elem;
};

template <typename T>
MyIterator<T>::MyIterator()
    : first_elem(nullptr), last_elem(nullptr), curr_elem(nullptr) {}

template <typename T>
MyIterator<T>::MyIterator(pointer _first_elem, pointer _last_elem, bool last)
    : first_elem(_first_elem),
      last_elem(_last_elem),
      curr_elem(last ? _last_elem : _first_elem) {}

#endif /* MY_ITERATOR_HPP */
