#pragma once

#include <vector>

#include "animal.h"

class Population {
 private:
  std::vector<Animal> individuals;
  unsigned max_population;

 public:
  /// @brief constructor for empty population
  /// @param max_population maximum population
  Population(unsigned max_population);

  /// @brief returns the number of individuals
  std::size_t size();

  /// @brief add an individual to the population
  /// @param individual individual to be added
  void add_individual(Animal individual);

  /// @brief runs `increase_age` on all individuals and runs the logic connected
  /// with it (birth, death)
  void increase_age();

  /// @brief remove dead individuals
  void remove_dead();
};