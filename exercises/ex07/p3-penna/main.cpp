#include <iomanip>
#include <iostream>

#include "animal.h"
#include "population.h"

#define N_MAX 10000

int main() {
  Genome genome;
  Animal default_individual(genome);
  Population population(N_MAX);

  for (unsigned n = 0; n < 1000; ++n)
    population.add_individual(default_individual);

  Genome::mutation_rate = 2;
  Animal::reproductive_age = 8;
  Animal::bad_mutation_threshold = 2;

  std::cout << std::setw(15) << "time," << std::setw(15) << "population"
            << std::endl;

  for (unsigned t = 0; t < 10000; ++t) {
    std::cout << std::setw(15) << t << "," << std::setw(15) << population.size()
              << std::endl;
    population.increase_age();
  }
}