#include "animal.h"

unsigned Animal::bad_mutation_threshold = 10;
unsigned Animal::reproductive_age = 3;

Animal::Animal(const Animal& animal)
    : age(animal.age),
      genome(animal.genome),
      bad_mutation_ctr(animal.bad_mutation_ctr) {}

Animal::Animal(Genome& _genome) : genome(_genome) {}

void Animal::increase_age(unsigned population, unsigned max_population) {
  gave_birth = false;
  if (is_dead) return;

  bad_mutation_ctr += genome[age];

  if ((bad_mutation_ctr >= bad_mutation_threshold) ||
      ((max_population - population) <= (rand() % max_population))) {
    is_dead = true;
    return;
  }

  gave_birth = age > reproductive_age;
  ++age;
}

Animal Animal::reproduce() {
  Genome new_genome = genome.reproduce();
  return Animal(new_genome);
}