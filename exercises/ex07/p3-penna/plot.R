library(ggplot2)
library(readr)

data <- read_csv("population.csv")

plot <- ggplot(data, aes(x = time, y = population)) +
  geom_point() +  # for a scatter plot
  labs(title = "Fig 1.", x = "time", y = "population")

ggsave("population.png", plot = plot, width = 8, height = 6, dpi = 300)