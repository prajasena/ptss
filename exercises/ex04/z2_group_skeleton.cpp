/* Programming Techniques for Scientific Simulations
 * Exercise 4.1
 */

#include <complex>
#include <iostream>

enum Z2 { Plus, Minus };

// return 1 (multiplicative identity) for any type
template <class T>
T identity_element() {
  return T(1);
}

// specializes identity_element for Z2
template <>
Z2 identity_element<Z2>() {
  return Plus;
}

// returns a times b in Z2
Z2 operator*(const Z2 a, const Z2 b) {
  if (a == Plus) return b;
  if (b == Plus) return Minus;
  return Plus;
}

std::ostream& operator<<(std::ostream& os, const Z2 a) {
  os << (a == Plus ? "+" : "-");
  return os;
}

// multiplies a with b
// type of a must have operator-(a) implemented
template <class T>
T operator*(const T a, const Z2 b) {
  return b == Plus ? a : -a;
}

template <class T>
T operator*(const Z2 b, const T a) {
  return a * b;
}

// returns a multiplied n times with itself
// type of a must have the * operator implemented
template <class T>
T mypow(T a, unsigned int n) {
  if (n == 0) return identity_element<T>();
  return a * mypow(a, n - 1);
}

int main() {
  // Some testing: feel free to add your own!
  std::cout << "Plus*Minus = " << Plus * Minus << std::endl;
  std::cout << "Plus*-1*Minus = " << Plus * -1 * Minus << std::endl;
  std::cout << "(1.+3.)*mypow(Minus,4) = " << (1. + 3.) * mypow(Minus, 4)
            << std::endl;

  for (unsigned i = 0; i < 7; ++i)
    std::cout << "Plus^" << i << " = " << mypow(Plus, i) << std::endl;
  for (unsigned i = 0; i < 7; ++i)
    std::cout << "Minus^" << i << " = " << mypow(Minus, i) << std::endl;
  for (unsigned i = 0; i < 7; ++i)
    std::cout << "2^" << i << " = " << mypow(2.0, i) << std::endl;

  // For complex numbers
  std::cout << "Plus*-std::complex<double>(1., 2.)*Minus = "
            << Plus * -std::complex<double>(1., 2.) * Minus << std::endl;
  std::cout << "std::complex<double>(1., 1.)*mypow(Minus,4) = "
            << std::complex<double>(1., 1.) * mypow(Minus, 4) << std::endl;
  for (unsigned i = 0; i < 7; ++i)
    std::cout << "std::complex<double>(1., 1.)^" << i << " = "
              << mypow(std::complex<double>(1., 1.), i) << std::endl;

  return 0;
}
