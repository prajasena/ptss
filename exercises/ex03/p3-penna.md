# Problem 3.3

*summary:*

- setup: discrete time $t \in \mathbb{N}$, $array of $N(t)$ individuals, bitstring of length $B$ per individual, parameters $T$, $R$, $M$
- birth: every individual with age $a$ generates one baby if $a > R$
- aging: every individual with age $a$ gets a mutation added if $B_a = 1$
- death: individual with $n$ mutations dies if $n > T$
- genome generation: parents genome but with $M$ randomly changed bits

*representation:*

individuals as structs with fields `genome` (some unsigned type or even array of them depending on size of $B$), `mutation_ctr`, `birth_year`