# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.22

# Delete rule output on recipe failure.
.DELETE_ON_ERROR:

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:

# Disable VCS-based implicit rules.
% : %,v

# Disable VCS-based implicit rules.
% : RCS/%

# Disable VCS-based implicit rules.
% : RCS/%,v

# Disable VCS-based implicit rules.
% : SCCS/s.%

# Disable VCS-based implicit rules.
% : s.%

.SUFFIXES: .hpux_make_needs_suffix_list

# Command-line flag to silence nested $(MAKE).
$(VERBOSE)MAKESILENT = -s

#Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /opt/local/bin/cmake

# The command to remove a file.
RM = /opt/local/bin/cmake -E rm -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /Users/sambird/Documents/ETH_Teaching/pt1_hs24/lecture/exercises/ex03/solutions/simpson_cmake

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /Users/sambird/Documents/ETH_Teaching/pt1_hs24/lecture/exercises/ex03/solutions/simpson_cmake

# Include any dependencies generated for this target.
include integrator/CMakeFiles/simpson_lib.dir/depend.make
# Include any dependencies generated by the compiler for this target.
include integrator/CMakeFiles/simpson_lib.dir/compiler_depend.make

# Include the progress variables for this target.
include integrator/CMakeFiles/simpson_lib.dir/progress.make

# Include the compile flags for this target's objects.
include integrator/CMakeFiles/simpson_lib.dir/flags.make

integrator/CMakeFiles/simpson_lib.dir/simpson.cpp.o: integrator/CMakeFiles/simpson_lib.dir/flags.make
integrator/CMakeFiles/simpson_lib.dir/simpson.cpp.o: integrator/simpson.cpp
integrator/CMakeFiles/simpson_lib.dir/simpson.cpp.o: integrator/CMakeFiles/simpson_lib.dir/compiler_depend.ts
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/Users/sambird/Documents/ETH_Teaching/pt1_hs24/lecture/exercises/ex03/solutions/simpson_cmake/CMakeFiles --progress-num=$(CMAKE_PROGRESS_1) "Building CXX object integrator/CMakeFiles/simpson_lib.dir/simpson.cpp.o"
	cd /Users/sambird/Documents/ETH_Teaching/pt1_hs24/lecture/exercises/ex03/solutions/simpson_cmake/integrator && /Library/Developer/CommandLineTools/usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -MD -MT integrator/CMakeFiles/simpson_lib.dir/simpson.cpp.o -MF CMakeFiles/simpson_lib.dir/simpson.cpp.o.d -o CMakeFiles/simpson_lib.dir/simpson.cpp.o -c /Users/sambird/Documents/ETH_Teaching/pt1_hs24/lecture/exercises/ex03/solutions/simpson_cmake/integrator/simpson.cpp

integrator/CMakeFiles/simpson_lib.dir/simpson.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/simpson_lib.dir/simpson.cpp.i"
	cd /Users/sambird/Documents/ETH_Teaching/pt1_hs24/lecture/exercises/ex03/solutions/simpson_cmake/integrator && /Library/Developer/CommandLineTools/usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -E /Users/sambird/Documents/ETH_Teaching/pt1_hs24/lecture/exercises/ex03/solutions/simpson_cmake/integrator/simpson.cpp > CMakeFiles/simpson_lib.dir/simpson.cpp.i

integrator/CMakeFiles/simpson_lib.dir/simpson.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/simpson_lib.dir/simpson.cpp.s"
	cd /Users/sambird/Documents/ETH_Teaching/pt1_hs24/lecture/exercises/ex03/solutions/simpson_cmake/integrator && /Library/Developer/CommandLineTools/usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -S /Users/sambird/Documents/ETH_Teaching/pt1_hs24/lecture/exercises/ex03/solutions/simpson_cmake/integrator/simpson.cpp -o CMakeFiles/simpson_lib.dir/simpson.cpp.s

# Object files for target simpson_lib
simpson_lib_OBJECTS = \
"CMakeFiles/simpson_lib.dir/simpson.cpp.o"

# External object files for target simpson_lib
simpson_lib_EXTERNAL_OBJECTS =

integrator/libsimpson_lib.a: integrator/CMakeFiles/simpson_lib.dir/simpson.cpp.o
integrator/libsimpson_lib.a: integrator/CMakeFiles/simpson_lib.dir/build.make
integrator/libsimpson_lib.a: integrator/CMakeFiles/simpson_lib.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --bold --progress-dir=/Users/sambird/Documents/ETH_Teaching/pt1_hs24/lecture/exercises/ex03/solutions/simpson_cmake/CMakeFiles --progress-num=$(CMAKE_PROGRESS_2) "Linking CXX static library libsimpson_lib.a"
	cd /Users/sambird/Documents/ETH_Teaching/pt1_hs24/lecture/exercises/ex03/solutions/simpson_cmake/integrator && $(CMAKE_COMMAND) -P CMakeFiles/simpson_lib.dir/cmake_clean_target.cmake
	cd /Users/sambird/Documents/ETH_Teaching/pt1_hs24/lecture/exercises/ex03/solutions/simpson_cmake/integrator && $(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/simpson_lib.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
integrator/CMakeFiles/simpson_lib.dir/build: integrator/libsimpson_lib.a
.PHONY : integrator/CMakeFiles/simpson_lib.dir/build

integrator/CMakeFiles/simpson_lib.dir/clean:
	cd /Users/sambird/Documents/ETH_Teaching/pt1_hs24/lecture/exercises/ex03/solutions/simpson_cmake/integrator && $(CMAKE_COMMAND) -P CMakeFiles/simpson_lib.dir/cmake_clean.cmake
.PHONY : integrator/CMakeFiles/simpson_lib.dir/clean

integrator/CMakeFiles/simpson_lib.dir/depend:
	cd /Users/sambird/Documents/ETH_Teaching/pt1_hs24/lecture/exercises/ex03/solutions/simpson_cmake && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /Users/sambird/Documents/ETH_Teaching/pt1_hs24/lecture/exercises/ex03/solutions/simpson_cmake /Users/sambird/Documents/ETH_Teaching/pt1_hs24/lecture/exercises/ex03/solutions/simpson_cmake/integrator /Users/sambird/Documents/ETH_Teaching/pt1_hs24/lecture/exercises/ex03/solutions/simpson_cmake /Users/sambird/Documents/ETH_Teaching/pt1_hs24/lecture/exercises/ex03/solutions/simpson_cmake/integrator /Users/sambird/Documents/ETH_Teaching/pt1_hs24/lecture/exercises/ex03/solutions/simpson_cmake/integrator/CMakeFiles/simpson_lib.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : integrator/CMakeFiles/simpson_lib.dir/depend

