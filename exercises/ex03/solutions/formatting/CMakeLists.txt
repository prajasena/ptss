cmake_minimum_required(VERSION 3.15)
project(ex03-problem3.4)

# get "AppleClang" instead of "Clang" on CMAKE_CXX_COMPILER_ID
# when running macOS with clang provided by Xcode
if (POLICY CMP0025)
  cmake_policy(SET CMP0025 NEW)
endif()

# Check if the compiler is AppleClang
if(CMAKE_CXX_COMPILER_ID STREQUAL "AppleClang")
    message(FATAL_ERROR "AppleClang does not support <format> from C++20. Please use GNU GCC.")
endif()

# use C++20
set(CMAKE_CXX_STANDARD 20)

# setting compiler flags
add_compile_options(-Wall -Wextra -Wpedantic)

# define executable
add_executable(rndtable rndtable.cpp)
