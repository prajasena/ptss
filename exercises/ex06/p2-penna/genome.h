#include <stdint.h>

#include <cassert>

using genome_bits = unsigned __int128;

class Genome {
 private:
  genome_bits bits;

 public:
  /// @brief number of bits of the genome, must be <= 128 if using u128 for
  /// genome bits
  static constexpr unsigned genome_length = 128;

  /// @brief the number of bits that will be flipped when genome is reproduced
  static constexpr unsigned mutation_rate = 10;

  Genome();

  /// @brief construct `Genome` member from bits
  Genome(genome_bits bits);

  /// @brief copy constructor
  Genome(Genome& g);

  /// @brief returns the bits of the genome
  genome_bits get_bits();

  /// @brief get the mutation state of the n-th bit of the genome
  /// @param i index of the genome, must be smaller than genome_length
  /// @return true if there is a mutation
  bool operator[](unsigned i);

  /// @brief get the number of bad mutations, i.e. the count of 1 bits in the
  /// genome, until the i-th bit
  /// @return number of mutations
  unsigned mutations();

  /// @brief generates a new genome after reproduction i.e. copies the genome
  /// and flips `mutation_rate` bits with probability 1/2
  /// @return the new genome
  Genome reproduce();
};