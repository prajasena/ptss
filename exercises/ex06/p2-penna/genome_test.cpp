#include "genome.h"

#include <cassert>

bool default_genome_is_good();
bool gnome_copy_identical();

int main() {
  assert(default_genome_is_good());
  assert(gnome_copy_identical());
}

bool default_genome_is_good() { return Genome().mutations() == 0; }

bool gnome_copy_identical() {
  Genome parent(42);
  Genome child(parent);

  return parent.get_bits() == child.get_bits();
}