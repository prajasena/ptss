#ifndef TIMER_HPP
#define TIMER_HPP

#include <chrono> // for std::chrono::high_resolution_clock

/**
 *  A timer class for benchmarking code
 */
class Timer {
    public:
        /**
         *  Create a timer.
         *
         *  The timer can be reused.
         */
        Timer();

        /**
         *  Starts the timer.
         *  Throws an error if the timer is already running.
         */
        void start();

        /**
         *  Stops the timer.
         *  Throws an error if the timer is not running.
         */
        void stop();

        /**
         *  Returns the duration of the timer in seconds.
         *
         *  If the timer has been stopped, returns the duration between the last callings of `start()` and `stop()`.
         *  If the timer is currently running, returns the duration since the last time `start()` was called.
         *
         *  Throws an error if the timer was never started.
         */
        double duration() const;

    private:
        using clock_t = std::chrono::high_resolution_clock;
        using time_point_t = clock_t::time_point;

        bool was_started_;
        bool is_running_;
        time_point_t tstart_;
        time_point_t tend_;
};

#endif // !defined TIMER_HPP
