#include "timer.hpp"

#include <chrono> // for std::chrono::duration
#include <stdexcept> // for std::runtime_error

Timer::Timer() : was_started_(false), is_running_(false) {}

void Timer::start() {
    if (is_running_) {
        throw std::runtime_error("Cannot start timer as it is already running");
    }
    was_started_ = true;
    is_running_ = true;
    tstart_ = clock_t::now();
}

void Timer::stop() {
    if (!is_running_) {
        throw std::runtime_error("Cannot stop timer as it is not running");
    }
    tend_ = clock_t::now();
    is_running_ = false;
}

double Timer::duration() const {
    if (!was_started_) {
        throw std::runtime_error("Cannot get duration as timer was never started");
    }

    return std::chrono::duration<double>((is_running_ ? clock_t::now() : tend_) - tstart_).count();
}
