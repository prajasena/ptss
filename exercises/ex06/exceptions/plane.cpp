#include <iostream>   // for std::cout
#include <stdexcept>  // for std::runtime_error

class Plane {
 public:
  void start() { std::cout << "Plane started successfully." << std::endl; }

  void serve_food() { throw std::runtime_error("The food is not edible!"); }

  void land() { std::cout << "Plane landed successfully." << std::endl; }
};

int main() {
  Plane plane;
  plane.start();

  try {
    plane.serve_food();
  } catch (const std::runtime_error err) {
    std::cerr << err.what();
  }

  plane.land();
}
