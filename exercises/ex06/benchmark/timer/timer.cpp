#include "timer.hpp"

#include <chrono>
#include <stdexcept>

Timer::Timer() : has_stared(false) {}

void Timer::start() {
  if (has_stared) throw std::runtime_error("timer has already been started");

  start_time = std::chrono::system_clock::now();
  has_stared = true;
}

void Timer::stop() {
  if (!has_stared) throw std::runtime_error("timer hasn't been started");

  auto end_time = std::chrono::system_clock::now();
  std::chrono::duration<double> elapsed_time = end_time - start_time;
  elapsed_secs = elapsed_time.count();

  has_stared = false;
}

unsigned Timer::duration() {
  if (has_stared) throw std::runtime_error("timer hasn't been stopped");

  return elapsed_secs;
}