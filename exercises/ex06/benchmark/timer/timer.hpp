#include <chrono>

#pragma once

/// @brief timer for measuring the elapsed time
class Timer {
 private:
  bool has_stared;
  unsigned elapsed_secs;
  std::chrono::system_clock::time_point start_time;

 public:
  Timer();

  /// @brief starts the timer
  void start();

  /// @brief stops the timer
  void stop();

  /// @brief returns the number of elapsed seconds
  unsigned duration();
};