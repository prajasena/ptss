#include <cstdint>

/// @brief random number generator using the linear congruential method
class Generator {
 private:
  uint32_t seed;
  uint32_t prev_value;

 public:
  /// @brief constructor
  /// @param seed seed value (X_0)
  Generator(uint32_t seed);

  /// @brief generate new value
  uint32_t generate();

  /// @brief biggest possible output
  uint32_t max();
};