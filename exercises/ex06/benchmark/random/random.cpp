#include "random.hpp"

#include <cstdint>
#include <limits>

Generator::Generator(uint32_t _seed) : seed(_seed) {}

uint32_t Generator::generate() {
  // mod implied by overflow
  return 1664525 * prev_value + 1013904223;
}

uint32_t Generator::max() { return std::numeric_limits<uint32_t>::max(); }