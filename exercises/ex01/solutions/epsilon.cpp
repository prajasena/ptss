/*
 * Programming Techniques for Scientific Simulations I
 * Exercise 1.5
 */

#include <iostream> // for std::cout
#include <iomanip>  // for std::setprecision
#include <limits>   // for std::numeric_limits

using epsilon_t = double;

epsilon_t epsilon_approximate();
epsilon_t epsilon_limits();

int main() {
  // Note that there are different definitions of machine precision
  // see https://en.wikipedia.org/wiki/Machine_epsilon#Variant_definitions
  // in particular, the one presented in the lecture is different from the one in the C++ standard
  std::cout << std::setprecision(20)
            << "approximate: " << epsilon_approximate() << '\n'
            << "limits:      " << epsilon_limits()      << '\n';
}


// Calculate machine precision to a factor of 2
// see https://en.wikipedia.org/wiki/Machine_epsilon#Approximation
epsilon_t epsilon_approximate() {
  const epsilon_t one  = 1;
  const epsilon_t half = 0.5;
  epsilon_t epsilon = 1;
  while (one + half * epsilon != one) {
    epsilon *= half;
  }

  return epsilon;
}


epsilon_t epsilon_limits() {
  return std::numeric_limits<epsilon_t>::epsilon();
}
