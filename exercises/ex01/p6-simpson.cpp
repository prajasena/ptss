#include <iostream>

template <typename F>
double integrate(F f, double from, double to, unsigned bins);

int main() {
  const auto f = [](double x) -> double { return sin(x); };
  std::cout << integrate(f, 0, M_PI, 10);
}

template <typename F>
double integrate(F f, double from, double to, unsigned bins) {
  double sum = 0;
  const double del_x = (to - from) / bins;

  for (unsigned int i = 0; i < bins; ++i) {
    const double x_i = from + i * del_x;
    sum += f(x_i);
    sum += 4 * f(x_i + del_x / 2);
    sum += f(x_i + del_x);
  }

  return del_x / 6 * sum;
}