#include <iostream>
#include <limits>

int main() {
  // from the NumCSE lecture doc by R. Hiptmair
  std::cout.precision(15);
  std::cout << std::numeric_limits<double>::epsilon() << std::endl;
}